# Code examples

### Directives

#### filterMult

`downwardly dependent filters`

#### chatSystemMessage

`system messages service`

#### fileLoader

`file loader with drag'n'drop feature`

### Modules

#### cabinet

`user profile page`

#### feed 

`start page with all requests sorted by categories`
