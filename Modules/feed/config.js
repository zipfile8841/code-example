export default function Config($stateProvider) {
	$stateProvider.state("feed", {
		url: "/feed",
		template: require("./feedPage/feed.haml"),
		controller: "feedCtrl",
		controllerAs: "fd"
	});
}

Config.$inject = ["$stateProvider"];