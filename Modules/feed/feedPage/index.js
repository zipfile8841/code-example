import "./feed.scss";

export default class FeedPageController {
	constructor($q, $state, Requests) {
		this.$q = $q;
		this.$state = $state;
		this.Requests = Requests;
		this.requestsCount();
	}

	redirect(filter) {
		this.$state.go("^.requests-list", {filter});
	}

	requestsCount() {
		this.$q.all({
			critical: this.Requests.one("count").get({only_critical: true}),
			outdated: this.Requests.one("count").get({only_outdated: true}),
			forToday: this.Requests.one("count").get({only_for_today: true}),
			forWeek: this.Requests.one("count").get({only_for_week: true}),
			forInfinite: this.Requests.one("count").get({only_infinite: true}),
			forAll: this.Requests.one("count").get({states: "draft,new,progress,agreement,approved,done,signing"})
		})
			.then((res) => {
				this.critical = res.critical.count;
				this.outdated = res.outdated.count;
				this.forToday = res.forToday.count;
				this.forWeek = res.forWeek.count;
				this.forInfinite = res.forInfinite.count;
				this.forAll = res.forAll.count;
			});
	}
}
FeedPageController.$inject = ["$q", "$state", "Requests"];