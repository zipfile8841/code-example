import angular from "angular";
import uiRouter from "angular-ui-router";
import toaster from "angularjs-toaster";

import Config from "./config";
import FeedCtrl from "./feedPage";

export default angular
	.module("kondr.feed", [uiRouter, toaster])
	.config(Config)
	.controller("feedCtrl", FeedCtrl)
	.name;