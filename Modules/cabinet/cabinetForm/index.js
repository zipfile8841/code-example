import "./cabinet.scss";

import { keyBy } from "lodash";

export default class cabinetFormCtrl {
	constructor($q, $timeout, toaster, JRPC, Identity, Roles) {
		this.$q = $q;
		this.$timeout = $timeout;
		this.toaster = toaster;
		this.JRPC = JRPC;
		this.Identity = Identity;
		this.Roles = Roles;

		this.resetFields();
		this.getData();
	}

	getData() {
		this.$q.all({
			user: this.Identity.getData(),
			roles: this.Roles.getList()
		})
			.then(res => {
				this.user_data = res.user;
				this.roles = keyBy(res.roles, "code");
			});
	}

	resetFields() {
		this.oldPass = "";
		this.newPass = "";
		this.confPass = "";
	}


	saveData() {
		// prepare password for validating
		this.oldPass = String(this.oldPass).trim();
		this.newPass = String(this.newPass).trim();
		this.confPass = String(this.confPass).trim();

		if (this.oldPass.length === 0) {
			this.toaster.pop("error", "Ошибка", "Введите старый пароль!", 3000);
			return;
		}

		if (this.newPass !== this.confPass) {
			this.toaster.pop("error", "Ошибка", "Пароли не совпали!", 3000);
			return;
		}

		if (this.newPass.length < 8) {
			this.toaster.pop("error", "Ошибка", "Пароль слишком короткий! (минимум 8 символов)", 3000);
			return;
		}

		// если не закрепить контекст, то потеряем его при передаче POJO в subscribe ниже
		const { $timeout, toaster } = this;

		// send query on success validation
		this.JRPC.query("changeCurrentUserPassword", { old: this.oldPass, new: this.newPass })
			.subscribe({
				error(err) {
					if (err.code === 1404) {
						$timeout(() => toaster.pop("error", "Ошибка", "Старый пароль введён неверно", 3000), 100);
					}
				}
			});
	}

}

cabinetFormCtrl.$inject = ["$q", "$timeout", "toaster", "JRPC", "Identity", "Roles"];