export default function Config($stateProvider) {
	$stateProvider.state("cabinet", {
		url: "/cabinet",
		template: require("./cabinetForm/cabinetForm.haml"),
		controller: "cabinetFormCtrl",
		controllerAs: "cf"
	});
}
Config.$inject = ["$stateProvider"];