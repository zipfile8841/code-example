import angular from "angular";
import uiRouter from "angular-ui-router";
import toaster from "angularjs-toaster";

import Config from "./config";
import cabinetFormCtrl from "./cabinetForm";

export default angular
	.module("kondr.cabinet", [uiRouter, toaster])
	.config(Config)
	.controller("cabinetFormCtrl", cabinetFormCtrl)
	.name;