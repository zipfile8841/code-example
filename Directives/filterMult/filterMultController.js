import _ from 'lodash';
import $ from 'jquery';

export default class filterMultController {
	constructor($q, $scope, Brands, Buildings, Cities, Stores) {
		this.model = $scope.model;
		this.model.city_ids = [];
		this.model.building_ids = [];
		this.model.brand_ids = [];
		this.$q = $q;
		this.Stores = Stores;
		this.Brands = Brands;
		this.Buildings = Buildings;
		this.Cities = Cities;
		this.request();
	}

	cityChange(handler) {
		this.model.brand_ids = [];
		this.model.building_ids = [];
		this.model.store_ids = [];
		this.buildings = [];
		this.stores = [];
		this.brands = [];
		const city_ids = this.model.city_ids;
		const building_ids = [];
		const brand_ids = [];
		let currentBuildings = [];
		let currentStores = [];
		let currentBrands = [];

		if (city_ids.length !== 0) {
			for (const c of city_ids) {
				currentBuildings = _.filter(this.allBuildings, ['city_id', c]);
				for (const b of currentBuildings) {
					this.buildings.push(b);
				}
			}
		} else {
			this.buildings = this.allBuildings;
		}

		for (const b of this.buildings) {
			building_ids.push(b.id);
		}
		for (const b of building_ids) {
			currentStores = _.filter(this.allStores, ['building_id', b]);
			for (const s of currentStores) {
				this.stores.push(s);
			}
		}
		this.cityStores = this.stores;

		for (const s of this.stores) {
			brand_ids.push(s.brand_id);
		}
		for (const b of brand_ids) {
			currentBrands = _.filter(this.allBrands, ['id', b]);
			for (const br of currentBrands) {
				this.brands.push(br);
			}
		}
		this.brands = _.uniq(this.brands);
		this.cityBrands = this.brands;

		if (handler) {
			this.rewriteLocalStorage();
		}
		setTimeout(this.reloadPicker, 0);
	}

	buildingChange(handler) {
		if (this.model.city_ids.length === 0) {
			this.cityStores = this.allStores;
			this.cityBrands = this.allBrands;
		}
		this.model.store_ids = [];
		this.model.brand_ids = [];
		this.brands = [];
		this.stores = [];
		const brand_ids = [];
		const building_ids = this.model.building_ids;
		let currentStores = [];
		let currentBrands = [];
		if (building_ids.length !== 0) {
			for (const b of building_ids) {
				currentStores = _.filter(this.cityStores, ['building_id', b]);
				for (const s of currentStores) {
					this.stores.push(s);
				}
			}
		} else {
			this.stores = this.cityStores;
		}
		this.buildingStores = this.stores;

		for (const s of this.stores) {
			brand_ids.push(s.brand_id);
		}
		for (const b of brand_ids) {
			currentBrands = _.filter(this.cityBrands, ['id', b]);
			for (const br of currentBrands) {
				this.brands.push(br);
			}
		}
		this.brands = _.uniq(this.brands);

		if (handler) {
			this.rewriteLocalStorage();
		}
		setTimeout(this.reloadPicker, 0);
	}

	brandChange(handler) {
		if (this.model.city_ids.length === 0) {
			this.cityStores = this.allStores;
		}
		if (this.model.building_ids.length === 0) {
			this.buildingStores = this.cityStores;
		}
		this.model.store_ids = [];
		this.stores = [];
		const brand_ids = this.model.brand_ids;
		let currentStores = [];
		if (brand_ids.length !== 0) {
			for (const b of brand_ids) {
				currentStores = _.filter(this.buildingStores, ['brand_id', b]);
				for (const s of currentStores) {
					this.stores.push(s);
				}
			}
		} else {
			this.stores = this.buildingStores;
		}

		if (handler) {
			this.rewriteLocalStorage();
		}
		setTimeout(this.reloadPicker, 0);
	}

	storeChange(handler) {
		if (handler) {
			this.rewriteLocalStorage();
		}
	}

	request() {
		this.Stores.getList()
			.then((res) => {
				this.stores = _.keyBy(res, 'id');
				this.building_ids = (_.uniq(_.map(res, 'building_id'))).join(',');
				this.brand_ids = (_.uniq(_.map(res, 'brand_id'))).join(',');
				this.$q.all({
					brands: this.Brands.getList({ ids: this.brand_ids }),
					buildings: this.Buildings.getList({ ids: this.building_ids })
				})
					.then((res) => {
						this.allBrands = this.brands = _.keyBy(res.brands, 'id');
						this.buildings = _.keyBy(res.buildings, 'id');
						this.city_ids = (_.uniq(_.map(res.buildings, 'city_id'))).join(',');
						this.Cities.getList({ ids: this.city_ids })
							.then((res) => {
								this.cities = _.keyBy(res, 'id');
								this.storesCreate(this.stores);
								this.buildingsCreate(this.buildings);
								if ((localStorage.getItem('city') !== null) && (localStorage.getItem('city') !== '')) {
									this.model.city_ids = localStorage.getItem('city').split(',')
										.map(Number);
									this.cityChange(false);
								}
								if ((localStorage.getItem('building') !== null) && (localStorage.getItem('building') !== '')) {
									this.model.building_ids = localStorage.getItem('building').split(',')
										.map(Number);
									this.buildingChange(false);
								}
								if ((localStorage.getItem('brand') !== null) && (localStorage.getItem('brand') !== '')) {
									this.model.brand_ids = localStorage.getItem('brand').split(',')
										.map(Number);
									this.brandChange(false);
								}
								if ((localStorage.getItem('store') !== null) && (localStorage.getItem('store') !== '')) {
									this.model.store_ids = localStorage.getItem('store').split(',')
										.map(Number);
									this.storeChange(false);
								}

								setTimeout(this.reloadPicker, 0);
							});
					});
			});
	}

	buildingsCreate(buildings) {
		for (const b in buildings) {
			// if (buildings[b].hasOwnProperty()) {
			buildings[b].city_name = this.cities[buildings[b].city_id].name;
			buildings[b].building_name = buildings[b].name;
			// }
		}
		this.allBuildings = this.buildings = _.sortBy(buildings, 'building_name', 'city_name');
	}

	storesCreate(stores) {
		for (const s in stores) {
			// if (stores[s].hasOwnProperty()) {
			stores[s].brand_name = this.brands[stores[s].brand_id].name;
			stores[s].building_name = this.buildings[stores[s].building_id].name;
			stores[s].store_code = stores[s].code;
			stores[s].city_name = this.cities[this.buildings[stores[s].building_id].city_id].name;
			// }
		}
		this.allStores = this.stores = _.sortBy(stores, 'brand_name', 'city_name', 'building_name', 'store_code');
	}

	reloadPicker() {
		$('.selectpicker_dir').selectpicker('destroy');
		$('.selectpicker_dir').selectpicker();
		$('.bootstrap-select').click(function () {
			if ($(this).hasClass('open')) {
				$(this).removeClass('open');
			} else {
				$(this).addClass('open');
			}
		});
		$('.selectpicker_dir').selectpicker('render');
	}
}

filterMultController.$inject = ['$q', '$scope', 'Brands', 'Buildings', 'Cities', 'Stores'];