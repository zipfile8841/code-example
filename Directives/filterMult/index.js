import controller from './filterMultController';
import template from './filterMult.haml';
export default function () {
	return {
		replace: true,
		template,
		controller,
		controllerAs: 'filter_mult',
		scope: {
			model: '='
		}
	};
}