import controller from './fileLoaderController';
import template from './fileLoader.haml';

export default function () {
	return {
		replace: true,
		template,
		controller,
		controllerAs: 'file_loader',
		scope: {
			model: '=',
			empty: '=',
			overlay: '=',
			loading: '<'
		}
	};
}
