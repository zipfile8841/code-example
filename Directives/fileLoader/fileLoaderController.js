import _ from 'lodash';
export default class fileLoaderController {
	constructor($scope) {
		this.$scope = $scope;
		this.model = $scope.model;
		this.dragMessage = 'Перетащите файл для загрузки';
		this.dropActive = false;
		this.zIndex = 900;

		// инструменты обработки drag and drop
		this.fileZone = document.querySelector('#file-zone');
		this.fileZone.addEventListener('dragleave', this.handleDragLeave.bind(this), true);
		this.fileZone.addEventListener('dragover', this.handleDragOver.bind(this), true);
		this.fileZone.addEventListener('drop', this.handleDrop.bind(this), true);
		this.page = document.querySelector('#page');
		window.addEventListener('dragenter', this.handleWindowEnter.bind(this), true);
		this.page.addEventListener('dragover', this.handleWindowOver, true);
		this.page.addEventListener('drop', this.handleWindowDrop.bind(this), true);
		this.page.addEventListener('dragleave', this.handleWindowLeave.bind(this), true);

		$scope.$on('$destroy', this.destroy.bind(this));
	}

	changeFileName(e) {
		if (e.target.files[0]) {
			this.model = e.target.files[0].name;
			this.$scope.empty = false;
		} else {
			this.model = 'Файл не выбран';
			this.$scope.empty = true;
		}
		this.$scope.$apply();
	}

	// обработчик попадания курсора с файлом на страницу
	handleWindowEnter(e) {
		e.stopPropagation();
		e.preventDefault();
		this.$scope.overlay = false;
		this.zIndex = 1000;
		this.$scope.$apply();
	}

	// обработчик покидания курсора с файлом страницы
	handleWindowLeave(e) {
		e.stopPropagation();
		e.preventDefault();
		this.zIndex = 900;
		this.$scope.overlay = true;
		this.$scope.$apply();
	}

	// обработчик нахождения курсора с файлом над страницей
	handleWindowOver(e) {
		e.stopPropagation();
		e.preventDefault();
	}

	// обработчик дропа файла на страницу
	handleWindowDrop(e) {
		e.stopPropagation();
		e.preventDefault();
		this.$scope.overlay = true;
		this.$scope.$apply();
	}

	// обработчик покидания курсора с файлом зоны загрузки
	handleDragLeave(e) {
		e.stopPropagation();
		e.preventDefault();
		this.dragMessage = 'Перетащите файл для загрузки';
		this.dropActive = false;
		this.$scope.$apply();
	}

	// обработчик нахождения курсора с файлом в зоне загрузки
	handleDragOver(e) {
		e.stopPropagation();
		e.preventDefault();
		this.dragMessage = 'Отпустите для загрузки';
		this.dropActive = true;
		this.$scope.$apply();
	}

	// обработчик дропа файла в зону загрузки
	handleDrop(e) {
		e.stopPropagation();
		e.preventDefault();
		if (e.dataTransfer.files[0]) {
			this.model = e.dataTransfer.files[0].name;
		}
		document.getElementById('file').files = e.dataTransfer.files;
		this.$scope.empty = false;
		this.$scope.overlay = true;
		this.dragMessage = 'Перетащите файл для загрузки';
		this.dropActive = false;
		this.$scope.$apply();
	}

	// снятие обработчиков событий при деинициализации
	destroy() {
		this.fileZone.removeEventListener('dragleave', this.handleDragLeave, true);
		this.fileZone.removeEventListener('dragover', this.handleDragOver, true);
		this.fileZone.removeEventListener('drop', this.handleDrop, true);
		window.removeEventListener('dragenter', this.handleWindowEnter, true);
		this.page.removeEventListener('dragover', this.handleWindowOver, true);
		this.page.removeEventListener('drop', this.handleWindowDrop, true);
		this.page.removeEventListener('dragleave', this.handleWindowLeave, true);
	}
}

fileLoaderController.$inject = ['$scope'];