import controller from './chatSystemMessageController';
import template from './chatSystemMessage.haml';

export default function () {
	return {
		replace: true,
		template,
		controller,
		controllerAs: 'sys_chat_mes',
		scope: {
			model: '=',
			fields: '=',
			roles: '='
		}
	};
}