import _ from 'lodash';
import moment from 'moment';

export default class chatSystemMessageController {
	constructor($scope, RequestsFieldsTranslator, Branches, RequestStates, Priorities, Roles, $q) {
		this.comment = $scope.model;
		this.items = [];

		$q.all({
			branches: Branches.getList(),
			states: RequestStates.getList(),
			priorities: Priorities.getList(),
			roles: Roles.getList()
		})
			.then((res) => {
				const branchCollection = _.keyBy(res.branches, 'code');
				const priorityCollection = _.keyBy(res.priorities, 'code');
				const stateCollection = _.keyBy(res.states, 'code');
				this.roles = _.keyBy(res.roles, 'code');

				for (const f in this.comment.fields) {
					if (this.comment.fields.hasOwnProperty(f)) {
						let [to, from] = [this.comment.fields[f].to, this.comment.fields[f].from];

						switch (f) {
							case 'priority':
								from = priorityCollection[this.comment.fields[f].from].name;
								break;
							case 'state':
								from = stateCollection[this.comment.fields[f].from].name;
								to = stateCollection[this.comment.fields[f].to].name;
								break;
							case 'branch':
								from = branchCollection[this.comment.fields[f].from].name;
								to = branchCollection[this.comment.fields[f].to].name;
								break;
							case 'complete_at':
								if (this.comment.fields[f].from) {
									from = moment(this.comment.fields[f].from).format('DD.MM.YYYY');
								}
								if (this.comment.fields[f].to) {
									to = moment(this.comment.fields[f].to).format('DD.MM.YYYY');
								}
								break;
						}

						if (from === null) {
							from = '"пусто"';
						}

						const adding = {
							from,
							to,
							name: RequestsFieldsTranslator.translate(f)
						};

						this.items.push(adding);
					}
				}
			});
	}
}

chatSystemMessageController.$inject = ['$scope', 'RequestsFieldsTranslator', 'Branches', 'RequestStates', 'Priorities', 'Roles', '$q'];
